# Déploiement Rancher UI en HTTPS avec Let's Encrypt - Docker-compose

## Environnement 

- Système d'exploitation : 
    - Serveur : `Ubuntu 16.04 Server`
    - Hébergement : [OVH] 
        - VPS SSD 2 (4 GO RAM, SSD 20 GO)
        - VPS SSD 1 (2 GO RAM, SSD 10 GO)
- Outils :
    - [Docker]
    - [Docker-compose]
    - [Rancher UI]
    - [Let's Encrypt]

## Prérequis

- Avoir deux serveurs à sa disposition, l'un servant à héberger notre instance de [Rancher UI] (qui utilise 1GO RAM pour fonctionner), et l'autre pour déployer nos applicatifs.
- Avoir préparé chacun des serveurs et sa machine en ayant suivi le TP : [Préparer sa machine à accéder à un serveur distant].
- Avoir un nom de domaine.

## Objectifs du tutoriel

- Installer [Rancher UI], pour orchestrer des containers Docker, via une interface graphique.

## Contexte

Aujourd'hui, le concept de container s'étend dans les entreprises, avec [Docker]. Cette technologie permet en outre de s'abstraire des problématiques liées à l'environnement. Fini les : "Je ne comprends pas ça marchait sur ma machine ... et quand je le passe sur l'environnement de Qualif ça ne marche plus...", eh oui puisque l'environnement dans lequel tourne notre applicatif est notre container (donc toujours le même) ! 
L'intérêt de [Rancher UI], va être de nous offrir à la fois un orchestrateur pour nos containers, mais aussi une gestion totalement transparente de la communication entre ces derniers sur différentes machines et tout ça via une belle interface graphique.

## Plan
- I - Préparation de notre infrastructure Rancher
- II - Installation docker et docker-compose
- III - Déploiement de Rancher UI + Let's Encrypt


### I - Préparation de notre infrastructure Rancher

Pour ce tutoriel, on va utiliser deux serveurs : 
- **Un serveur maître** : qui aura à se charge d'héberger [Rancher UI] et donc d'orchestrer les containers.
- **Un serveur esclave** : où l'on va déployer nos containers.

### II - Installation docker et docker-compose

#### Installation docker 

1. On commence par installer les paquets pour autoriser `apt` à utiliser un répertoire via HTTPS : 

```sh
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
```

2. On ajoute la clé [GPG] officielle de docker

```sh
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
On vérifie que l'emprunte de la clé est bien `9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88`.

```sh
$ sudo apt-key fingerprint 0EBFCD88

pub   4096R/0EBFCD88 2017-02-22
      Key fingerprint = 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
uid                  Docker Release (CE deb) <docker@docker.com>
sub   4096R/F273FCD8 2017-02-22
```

3. On met à jour les paquets `apt` : 

```sh
$ sudo apt-get update
```

4. On installe la dernière version de [Docker] :

```sh
$ sudo apt-get install docker-ce
```


**Note** : Si cela ne marche pas, comme pour moi sur l'un de mes serveurs (why ... I don't know) : 

```sh
$ curl -L https://get.docker.io |sh
```

5. On vérifie que docker est bien installé : 

```sh
$ sudo docker -V 
```

6. On s'ajoute aux utilisateurs pour ne plus avoir à mettre le `sudo` : 

```sh
$ sudo usermod -aG docker $(whoami)
```

#### Installation Docker-compose

1. On installe [Docker-compose] avec la commande `curl` : 

```sh
$ curl -L https://github.com/docker/compose/releases/download/1.14.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
```

Si jamais vous avez un `Permission denied` il faut que vous l'installiez en tant que superuser. Pour cela, lancez : 

```sh
$ sudo -i
```

Puis exécutez la commande `curl`, puis sortez avec `exit`. 

pour prendre la dernière version rendez-vous [ici](https://github.com/docker/compose/releases)

2. On applique les droits d'exécution au binaire : 

```sh
$ sudo chmod +x /usr/local/bin/docker-compose
```

3. On ajoute un alias pour se simplifier la vie, pour cela, on crée un fichier `.bash_aliases` contenant : 

```sh
alias dc='docker-compose'
```

4. Puis on source le fichier `.bashrc` qui charge ce fichier `.bash_aliases` : 

```sh
$ source .bashrc
```

Voilà notre serveur est prêt ! 


L'idée maintenant va être, avec un fichier `docker-compose.yml`, d'avoir un [Rancher UI] avec un [Proxy NGINX]

### III - Déploiement de Rancher UI + Let's Encrypt (version Nginx)

On va maintenant déployer notre [Rancher UI]. 

Pour commencer, on va créer un petit répertoire pour mettre tout ce dont on aura besoin : 

```sh
$ mkdir rancher
``` 

On se place dans le dossier :

```sh
$ cd rancher
```
#### a - Avec NGINX
On va maintenant définir notre fichier `docker-compose.yml`, qui va en plus d'installer [Rancher UI], rajouter un petit **proxy Nginx** et docker **Let's Encrypt** pour avoir du HTTPS sans avoir à payer de certificats.

**Note** : Vous allez avoir besoin d'une entrée DNS pour réaliser cette partie. Votre host que vous spécifiez doit pointer vers votre serveur ! Pour moi, l'host est `rancher.guittonalexandre.me`.
Voici le fichier : 

```sh
nginx-proxy:
  image: jwilder/nginx-proxy:latest
  ports:
    - "80:80"
    - "443:443"
  volumes:
    - '/home/docker/nginx-proxy/ssl:/etc/nginx/certs:ro'
    - '/etc/nginx/vhost.d'
    - '/usr/share/nginx/html'
    - '/var/run/docker.sock:/tmp/docker.sock:ro'
letsencrypt-nginx-proxy-companion:
  image: jrcs/letsencrypt-nginx-proxy-companion:latest
  volumes_from:
    - nginx-proxy
  volumes:
    - '/home/docker/nginx-proxy/ssl:/etc/nginx/certs:rw'
    - '/var/run/docker.sock:/var/run/docker.sock:ro'
rancher-server:
  image: rancher/server:latest
  environment:
    VIRTUAL_PORT: 8080
    VIRTUAL_HOST: rancher.guittonalexandre.me
    LETSENCRYPT_HOST: rancher.guittonalexandre.me
    LETSENCRYPT_EMAIL: erdrix@gmail.com
  volumes:
    - '/home/docker/rancher-server/mysql:/var/lib/mysql:rw'
```

On vérifie que tout est bon dans notre `docker-compose.yml` : 

```sh
$ dc config
```

On récupère l'ensemble des images nécessaires : 

```sh
$ dc pull
```

Puis on installe toute la stack : 
```sh
$ dc up -d
Creating withnginx_rancher-server_1 ...
Creating withnginx_nginx-proxy_1 ...
Creating withnginx_rancher-server_1
Creating withnginx_rancher-server_1 ... done
Creating withnginx_letsencrypt-nginx-proxy-companion_1 ...
Creating withnginx_letsencrypt-nginx-proxy-companion_1 ... done
```

On vérifie que tout est lancé :
```sh
$ dc ps
                    Name                                   Command               State                    Ports
---------------------------------------------------------------------------------------------------------------------------------
withnginx_letsencrypt-nginx-proxy-companion_1   /bin/bash /app/entrypoint. ...   Up
withnginx_nginx-proxy_1                         /app/docker-entrypoint.sh  ...   Up      0.0.0.0:443->443/tcp, 0.0.0.0:80->80/tcp
withnginx_rancher-server_1                      /usr/bin/entry /usr/bin/s6 ...   Up      3306/tcp, 8080/tcp
```

**Note :** ça peut prendre quelques minutes avant que [Rancher UI] ai fini de se configurer ! 

#### b - Avec Traefik

On va maintenant définir notre fichier `docker-compose.yml`, qui va en plus d'installer [Rancher UI], rajouter un petit **Traefik** et docker **Let's Encrypt** pour avoir du HTTPS sans avoir à payer de certificats.

**Note** : Vous allez avoir besoin d'une entrée DNS pour réaliser cette partie. Votre host que vous spécifiez doit pointer vers votre serveur ! Pour moi l'host est `rancher.guittonalexandre.me`.

##### Préparation de traefik

Pour commencer, on va créer un **réseau docker** à la main, qui va permettre de faire communiquer tous les services qui en ont besoin avec [Traefik].

```sh
$ docker network create traefik
```
dc 
On vérifie que notre réseau est bien créé : 

```sh
$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
12c9a6298445        bridge              bridge              local
4075212a4bdd        host                host                local
aee65b55bad1        none                null                local
d4c8758c4c95        traefik             bridge              local
```

On commence par créer un dossier `traefik` : 
```sh
$ mkdir traefik
```

On se place dans le dossier : 

```sh
$ cd traefik
```

On va ensuite commencer par créer notre fichier `traefik.toml` pour que traefik supporte [Let's Encrypt] : 
```sh
# defaultEntryPoints must be at the top because it should not be in any table below
defaultEntryPoints = ["http", "https"]

# Entrypoints, http and https
[entryPoints]

# http should be redirected to https
[entryPoints.http]
address = ":80"
[entryPoints.http.redirect]
entryPoint = "https"

# https is the default
[entryPoints.https]
address = ":443"
[entryPoints.https.tls]

# Enable ACME (Let's Encrypt): automatic SSL
[acme]
# caServer = "https://acme-staging.api.letsencrypt.org/directory"
email = "guittonalexandre@outlook.fr"
storage = "acme.json" # or "traefik/acme/account" if using KV store
entryPoint = "https"
onDemand = false
OnHostRule = true

[docker]
endpoint = "unix:///var/run/docker.sock"
domain = "example.com"
watch = true
exposedbydefault = false
```

On a désactivé la redirection HTTP et HTTPS, pour être sûr que la vérification de Let's Encrypt sur le domaine va marcher.

On crée ensuite le fichier `docker-compose.yml`, qui va permettre de lancer notre traefik : 
```sh
version: '2'

services:
  traefik:
    image: traefik:1.3.5-alpine
    restart: always
    command: --web --docker --logLevel=DEBUG
    networks:
      - traefik
    ports:
      - "8080:8080"
      - "80:80"
      - "443:443"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - $PWD/traefik.toml:/traefik.toml
      - $PWD/acme.json:/acme.json
    labels:
      traefik.enable: "true"
      traefik.backend: "traefik"
      traefik.docker.network: "traefik"
      traefik.frontend.rule: "Host:traefik.web.trycatchlearn.fr"
      traefik.port: "8080"
networks:
  traefik:
    external: true

```

On vérifie que le fichier est bon : 
```sh
$ dc config
```

On télécharge l'ensemble des images nécessaires : 
```sh
$ dc pull
Pulling traefik (traefik:1.3.2-alpine)...
1.3.5-alpine: Pulling from library/traefik
90f4dba627d6: Pull complete
bda6150311e3: Pull complete
cc089b2efdf4: Pull complete
f202136ee505: Pull complete
Digest: sha256:d58c70775a810e8392dcce47486bb13cc06a185a1cbb97190c11c568bd7d975c
Status: Downloaded newer image for traefik:1.3.5-alpine
```

On déploie notre [Traefik] : 
```sh
$ dc up -d
Creating traefik_traefik_1 ...
Creating traefik_traefik_1 ... done
```

On vérifie que tout est bon : 

```sh
$ dc ps
      Name                     Command               State                                Ports
-----------------------------------------------------------------------------------------------------------------------------
traefik_traefik_1   /entrypoint.sh --web --doc ...   Up      0.0.0.0:443->443/tcp, 0.0.0.0:80->80/tcp, 0.0.0.0:8080->8080/tcp
```

Une fois que c'est bon, on peut aller voir le dashbord de [Traefik] sur le port `8080` de votre serveur :
<p align="center">     
  <img alt="Treafik dashboard" src="https://gitlab.com/trycatchlearn/tutorials/rancher/raw/master/I_Rancher_https_traefik/img/dashboard_traefik.png"><br\>
    <b><ins>Image 1 : Traefik dashboard</ins></b>
</p>

#### On lance maintenant notre Rancher UI
Voici le fichier : 

```sh
version: '2.0'
services:
  rancher-server:
    image: rancher/server:latest
    networks:
      - traefik
    environment:
      VIRTUAL_PORT: 8080
      VIRTUAL_HOST: rancher.web.trycatchlearn.fr
    volumes:
      - '/home/docker/rancher-server/mysql:/var/lib/mysql:rw'
    expose:
      - "8080"
    labels:
      traefik.enable: "true"
      traefik.backend: "rancher"
      traefik.docker.network: "traefik"
      traefik.frontend.rule: "Host:rancher.web.trycatchlearn.fr"
      traefik.port: "8080"
    restart: always
networks:
  traefik:
    external: true
```

On vérifie que tout est bon dans notre `docker-compose.yml` : 

```sh
$ dc config
```

On récupère l'ensemble des images nécessaires : 

```sh
$ dc pull
```

Puis on installe toute la stack : 
```sh
$ dc up -d
Creating withtraefik_rancher-server_1 ...
Creating withtraefik_rancher-server_1 ... done
```

On vérifie que tout est lancé :
```sh
$ dc ps
                    Name                                   Command               State                    Ports
---------------------------------------------------------------------------------------------------------------------------------
withtraefik_rancher-server_1   /usr/bin/entry /usr/bin/s6 ...   Up      3306/tcp, 8080/tcp
```


**Note :** ça peut prendre quelques minutes avant que [Rancher UI] ai fini de se configurer ! 

### IV - Utilisation de Rancher

Notre Rancher est maintenant prêt à être utilisé. Nous allons maintenant voir le fonctionnement global de celui-ci. 

Pour ce faire, se rendre à l'adresse de votre rancher : `rancher.web.trycatchlearn.fr`

<p align="center">     
  <img alt="rancher_dashboard" src="https://gitlab.com/trycatchlearn/tutorials/rancher/raw/master/I_Rancher_https_traefik/img/rancher_dashboard.jpg"><br\>     
  <b><ins>Image 2 : Rancher dashboard</ins></b> 
</p>

#### Création de comptes

On va commencer par sécuriser l'accès à l'application en créant des comptes d'accès, pour cela se rendre dans l'onglet `ADMIN>ACCOUNT` 

<p align="center">     
  <img alt="rancher_dashboard_access_control" src="https://gitlab.com/trycatchlearn/tutorials/rancher/raw/master/I_Rancher_https_traefik/img/rancher_dashboard_access_control.jpg"><br\>     
  <b><ins>Image 3 : Access control rancher</ins></b> 
</p>

Il existe plusieurs types de comptes qui peuvent être utilisés pour sécuriser l'accès à Rancher : 
- [Active Directory](https://fr.wikipedia.org/wiki/Active_Directory)
- [Azure AD Authentication](https://docs.microsoft.com/en-us/azure/app-service/app-service-mobile-how-to-configure-active-directory-authentication)
- [GitHub](https://github.com/)
- [OpenLDAP](https://www.openldap.org/)
- [Shibboleth](https://www.shibboleth.net/)
- `Local` : qui est la solution que nous allons utiliser ici.

<p align="center">     
  <img alt="rancher_local_access_control" src="https://gitlab.com/trycatchlearn/tutorials/rancher/raw/master/I_Rancher_https_traefik/img/rancher_local_access_control.jpg"><br\>     
  <b><ins>Image 4 : Access control local rancher</ins></b> 
</p>

Remplir les champs et cliquer sur le bouton <img alt="enable_local_auth" src="https://gitlab.com/trycatchlearn/tutorials/rancher/raw/master/I_Rancher_https_traefik/img/enable_local_auth.jpg">

Vous pouvez toujours ajouter de nouveaux comptes en vous rendant dans `ADMIN>ACCOUNTS` :
<p align="center">     
  <img alt="rancher_add_account" src="https://gitlab.com/trycatchlearn/tutorials/rancher/raw/master/I_Rancher_https_traefik/img/rancher_add_account.jpg"><br\>     
  <b><ins>Image 5 : Ajout d'un compte</ins></b> 
</p>

#### Ajout d'un nouvel hôte

Le principal intérêt de Rancher est d'être un outil graphique permettant de gérer le déploiement de nos infrastructures via Docker sur plusieurs machines. Pour ce faire, il faut donc que les machines que l'on va orchestrer se déclarent au prêt de notre instance Rancher. 

La première étape est d'installer docker sur la machine. Pour cela, rancher offre un petit script d'installation que vous pouvez retrouver [ici](http://rancher.com/docs/rancher/v1.6/en/hosts/#supported-docker-versions).

Une fois Docker installé sur la machine, nous allons la déclarer auprès de Rancher. Il faut donc se rendre dans `INFRASTRUCTURE>Hosts` 
<p align="center">     
  <img alt="rancher_add_hosts" src="https://gitlab.com/trycatchlearn/tutorials/rancher/raw/master/I_Rancher_https_traefik/img/rancher_add_hosts.jpg"><br\>     
  <b><ins>Image 6 : Ajout d'un host</ins></b> 
</p>

Cliquer sur le bouton <img alt="rancher_add_hosts_button" src="https://gitlab.com/trycatchlearn/tutorials/rancher/raw/master/I_Rancher_https_traefik/img/rancher_add_hosts_button.jpg">

On va ensuite définir l'url de rancher qui va permettre à l'hôte de le contacter : 
<p align="center">     
  <img alt="rancher_host_registration" src="https://gitlab.com/trycatchlearn/tutorials/rancher/raw/master/I_Rancher_https_traefik/img/rancher_host_registration.jpg"><br\>     
  <b><ins>Image 7 : Enregistrement d'un hôte</ins></b> 
</p>

On copie ensuite la ligne de commande qui nous est fournie par rancher, que l'on exécute sur le serveur hôte que l'on veut déclarer. Une fois que tout s'est bien passé on voit que notre serveur est déclaré dans les liste des hôtes.

### VI - Bilbliographie 

[Enabling HTTPS termination in Traefik](http://niels.nu/blog/2017/traefik-https-letsencrypt.html) (30/06/2017)
[Déployer Rancher UI en HTTPS avec Docker](https://www.nicolashug.com/developpement/docker/deployer-rancher-ui-https-docker) (30/06/2017)

[Proxy NGINX]: <https://www.nginx.com/resources/admin-guide/reverse-proxy/>
[Docker]: <https://docs.docker.com/engine/installation/>
[Docker-Compose]: <https://docs.docker.com/compose/>
[Rancher UI]: <http://rancher.com/rancher>
[Let's Encrypt]: <https://letsencrypt.org/>
[Préparer sa machine à accéder à un serveur distant]: <>
[GPG]: <https://fr.wikipedia.org/wiki/GNU_Privacy_Guard>